var gulp = require('gulp');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var postcss = require('gulp-postcss');
var uncss = require('postcss-uncss');
var inlinesource = require('gulp-inline-source');
const concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "dist"
        }
    }).reload();
});


gulp.task('uncss', function () {
    var plugins = [
        uncss({
            html: ['http://localhost:3000/'],
            ignore: ['body.is-unknown-user','body.is-guest-user','body.is-free-user']
        })
    ];
    return gulp.src('dev/css/main.css')
        .pipe(postcss(plugins))
        .pipe(gulp.dest('dev/css/uncss'));
});

gulp.task('concat', function () {
    return gulp.src('dev/css/render_css/*.css')
        .pipe(concat('main.css'))
        .pipe(gulp.dest('dev/css/'));

});


gulp.task('watch', function (done) {
    browserSync.reload();
    console.log('reload!');
    done();
});

gulp.task('HTMLminify', function() {
    return gulp.src('dist/notready/index.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('inlinesource', function () {
    var options = {
        compress: true
    };

    return gulp.src('./dev/index.html')
        .pipe(inlinesource(options))
        .pipe(gulp.dest('./dist/notready'));
});

gulp.task('build', function (cb) {
    return gulp.src('dev/css/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dev/css/render_css'));
    cb();
});



gulp.task('default', gulp.parallel(['browser-sync'], function() {
    gulp.watch(['dev/css/scss/*.*'], gulp.series(['build']));
    gulp.watch(['dev/css/render_css/*.*'], gulp.series(['concat']));
    gulp.watch(['dev/css/*.*'], gulp.series(['uncss']));
    gulp.watch(['dev/css/uncss/*.*', 'dev/index.html'], gulp.series(['inlinesource']));
    gulp.watch(['dist/notready/*.*'], gulp.series(['HTMLminify']));
    gulp.watch(['dist/*.*'], gulp.series(['watch']));
}));